oc create secret docker-registry aws-ecr-pull-secret \
  --docker-server=712731808180.dkr.ecr.us-east-1.amazonaws.com \
  --docker-username=AWS \
  --docker-password=$(aws ecr get-login-password) 

oc secrets link default aws-ecr-pull-secret --for=pull
