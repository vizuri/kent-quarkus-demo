# Kent Quarkus Demo Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.
The purpose of the project is to demonstrator building a microservice container and deploying it on OpenShift with Helm

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw compile quarkus:dev
```

## Packaging and running the application

The application can be packaged using:
```
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 

```
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 

```
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/kent-quarkus-demo-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

## Provided Code

### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)


## Deploy on OpenShift
### Prerequisites
- OpenShift Command Line Interface (oc)
- Helm Command Line (helm)
- Kubernetes Command Line Interface (kubectl)


### Build the native executable
```
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

### Create a container from the native executable and tag it for your ECR Repository
```
docker build -f src/main/docker/Dockerfile.native -t 712731808180.dkr.ecr.us-east-1.amazonaws.com/kent-quarkus-demo:1.0 .
```

### Push the container to ECR.  Note: you must be logged into the ECR registry with docker credentials
```
docker push 712731808180.dkr.ecr.us-east-1.amazonaws.com/kent-quarkus-demo:1.0
```

### Deploy the container to OpenShift
- Login into OpenShift with either a service account token or user credentials
- Create or Navigate to a Project where you would like the helm chart deployed.
- Deploy the Chart
```
helm install kent-quarkus-demo src/main/kubernetes/kent-quarus-demo/
```
